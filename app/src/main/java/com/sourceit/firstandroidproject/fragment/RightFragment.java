package com.sourceit.firstandroidproject.fragment;

import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.sourceit.firstandroidproject.R;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

public class RightFragment extends Fragment {

    public static final int START = 1;
    public static final int STOP = 2;
    int time = 10;
    boolean isStarted;
    @BindView(R.id.info)
    TextView infoView;
    Handler h = new Handler() {
        @Override
        public void dispatchMessage(Message msg) {
            if (msg.what == START) {
                infoView.setText(String.valueOf(time));
                time--;
                if (time >= 0) {
                    h.sendEmptyMessageDelayed(START, 1_000);
                }
            } else if (msg.what == STOP) {
                stopTimer();
            }
        }
    };
    Unbinder unbinder;

    public static RightFragment newInstance() {
        return new RightFragment();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_info, container, false);
        unbinder = ButterKnife.bind(this, root);

        new Thread(new Runnable() {
            @Override
            public void run() {
                final String text = "data";
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }

                //1
                //handler

                //2
                infoView.post(new Runnable() {
                    @Override
                    public void run() {
                        infoView.setText(text);
                    }
                });

                //3
                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        infoView.setText(text);
                    }
                });



                startTimer();
            }
        }).start();

        new MyTask().execute(10);
        return root;
    }

    public void startTimer() {
        isStarted = true;
        h.sendEmptyMessage(START);
    }

    public void stopTimer() {
        isStarted = false;
        h.removeMessages(START);
    }


    public void setText(String str) {
        infoView.setText(str);
    }

    @OnClick(R.id.info)
    public void onNumberClick() {
        if (isStarted) {
            stopTimer();
        } else {
            startTimer();
        }
    }

    @Override
    public void onDestroyView() {
        unbinder.unbind();
        h.removeMessages(START);
        h.removeMessages(STOP);
        super.onDestroyView();
    }

    class MyTask extends AsyncTask<Integer, Long, String> {

        @Override
        protected void onPreExecute() {
            //main thread
            Toast.makeText(getContext(), "let's start", Toast.LENGTH_SHORT).show();
        }

        @Override
        protected String doInBackground(Integer... params) {
            int result = params[0];
            for (int i = 0; i < 10_000_000; i++) {
                result++;
                if (i % 1000 == 0) {
                    publishProgress((long) i);
                }
            }
            return String.valueOf(result);
        }

        @Override
        protected void onProgressUpdate(Long... values) {
            infoView.setText(String.valueOf(values[0]));
        }

        @Override
        protected void onPostExecute(String result) {
            //main thread
            infoView.setText(result);
        }
    }

}
