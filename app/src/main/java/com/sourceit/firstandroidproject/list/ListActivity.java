package com.sourceit.firstandroidproject.list;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.sourceit.firstandroidproject.R;

import org.w3c.dom.Text;

public class ListActivity extends AppCompatActivity {

    ListView listView;
//    ArticleAdapter adapter;
    BaseArticleAdapter adapter;
    Article[] articles = Generator.generate();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list);
        listView = (ListView) findViewById(R.id.list_view);

//        adapter = new ArticleAdapter(this, articles);
        adapter = new BaseArticleAdapter(this, Generator.generateList());
        listView.setAdapter(adapter);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Toast.makeText(ListActivity.this, articles[position].getTitle(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    class ArticleAdapter extends ArrayAdapter<Article> {

        public ArticleAdapter(Context context, Article[] articles) {
            super(context, R.layout.item_layout, articles);
        }

        @NonNull
        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            View root = getLayoutInflater().inflate(R.layout.item_layout, parent, false);
//            View rowView = LayoutInflater.from(getContext()).inflate(R.layout.item_layout, parent, false);
            TextView title = (TextView) root.findViewById(R.id.title);
            TextView text = (TextView) root.findViewById(R.id.text);

            Article article = getItem(position);
            title.setText(article.getTitle());
            text.setText(article.getText());
            return root;
        }
    }
}
