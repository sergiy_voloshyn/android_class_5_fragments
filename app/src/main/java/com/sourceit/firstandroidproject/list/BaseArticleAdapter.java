package com.sourceit.firstandroidproject.list;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.sourceit.firstandroidproject.R;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by wenceslaus on 16.01.18.
 */

public class BaseArticleAdapter extends BaseAdapter {

    private Context context;
    private List<Article> list;

    public BaseArticleAdapter(Context context, List<Article> list) {
        this.context = context;
        this.list = list;
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Article getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return list.get(position).hashCode();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder;
        View root = convertView;

        if (root == null) {
            root = LayoutInflater.from(context).inflate(R.layout.item_layout, parent, false);
            viewHolder = new ViewHolder(root);
            root.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) root.getTag();
        }

        Article article = getItem(position);
        viewHolder.title.setText(article.getTitle());
        viewHolder.text.setText(article.getText());

        return root;
    }

    static class ViewHolder {
        @BindView(R.id.title)
        TextView title;
        @BindView(R.id.text)
        TextView text;

        ViewHolder(View view) {
            ButterKnife.bind(this, view);
        }
    }

}
